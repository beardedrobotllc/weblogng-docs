.. _ref-architectural-overview:

=============================
Architectural Overview
=============================

Overview
------------------------------------

WeblogNG enables developers and operations engineers to understand the performance of mobile and html/javascript applications,
as experienced by the end-user in production.

.. image:: img/weblogng-architecture-overview.svg
    :width: 800 px
    :alt: WeblogNG Architecture Diagram - Overview

Data flows from instrumented applications to a dashboard via the following process:

#. developers instrument applications using a WeblogNG client library
#. when customers use an instrumented application, data is reported by applications via websockets or http to the WeblogNG metrics api
#. the WeblogNG metrics api collects the incoming metrics data by metric name

    #. the raw data is stored and made available for addition to a chart automatically
    #. the dashboard provides a chart-builder to configure a chart that transforms the collected metrics using an aggregation to help understand the performance indicated by that data in that time period:

        * Good - the 5th `percentile <http://en.wikipedia.org/wiki/Percentile>`_ of the data
        * Typical - the 50th `percentile <http://en.wikipedia.org/wiki/Percentile>`_ (`median <http://en.wikipedia.org/wiki/Median>`_), of the data
        * Poor - the 95th `percentile <http://en.wikipedia.org/wiki/Percentile>`_ of the data

    #. developers and operations engineers create and use WeblogNG dashboards to understand the
        end-user experience of their applications
    #. a dashboard will automatically update each chart with updated data as time passes

Supported Client Libraries
------------------------------------

WeblogNG publishes and supports client libraries for the following platforms:

* iOS
    * Requirements: iOS 6, 7, or 8
    * Get Started with :ref:`iOS <ref-client-library-ios>`
* Javascript
    * Requirements: a Javascript environment with WebSockets
    * Get Started with :ref:`Javascript <ref-client-library-javascript>`


Best Practices
------------------------------------

Instrumentation
^^^^^^^^^^^^^^^

Time all (potentially) long-running operations, such as service or asset-loading requests and large render operations.

Measure the size of files and key business object collections.

Metric Naming
^^^^^^^^^^^^^

WeblogNG supports metric names that use alpha-numeric characters, hyphens, and underscores.
i.e. characters that match a regex character class of: ``\w\d\-_``. Other characters will
be replaced with underscores.

Here are some examples of good metric names one might use while instrumenting an application's 'save' process:

* SampleApp-services-save-prepareRequest
* SampleApp-services-save-http
* SampleApp-services-save-handleResponse
* SampleApp-services-save-total
