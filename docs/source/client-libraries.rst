.. _ref-client-libraries:

================
Client Libraries
================

Overview
--------

Client libraries are used by the application to measure and report data in real-world usage.

Supported Platforms
-------------------

WeblogNG publishes and supports client libraries for the following platforms:

* iOS
    * Requirements: iOS 6, 7, or 8
    * Get Started with :ref:`iOS <ref-client-library-ios>`
* Javascript
    * Requirements: a Javascript environment
    * Get Started with :ref:`Javascript <ref-client-library-javascript>`
* Javascript with AngularJS
    * Requirements: AngularJS 1.2 or later, a Javascript environment
    * Get Started with :ref:`angular-weblogng <ref-client-library-angular-weblogng>`
