.. _ref-client-library-angular-weblogng:
.. meta::
   :description: Technical documentation for the WeblogNG AngularJS module for real user monitoring in AngularJS applications.
   :keywords: WeblogNG Javascript library Angular AngularJS
   :title: AngularJS Module for WeblogNG

*************************
WeblogNG AngularJS Module
*************************

.. contents::

Overview
=========================

WeblogNG provides an AngularJS module for WeblogNG that AngularJS applications can use to measure and report data in real-world usage easily.

angular-weblogng provides:

* automatic measurement and reporting of application load time
* automatic measurement of the number of active users
* automatic measurement and reporting of requests made with the standard AngularJS `$http service <https://docs.angularjs.org/api/ng/service/$http>`_
* easy access to the :ref:`WeblogNG Javascript Client Library<ref-client-library-javascript>`

Usage
=====

Using the WeblogNG client is very easy:

#. add the angular-weblogng dependency to your project
#. integrate the AngularJS module for WeblogNG into your application
#. create dashboard and charts with your metrics

Add the library to your project
-------------------------------

Option 1 - Bower
^^^^^^^^^^^^^^^^

Install the WeblogNG logger library via `bower <http://bower.io>`_ using::

   bower install angular-weblogng --save

The bower_components/angular-weblogng/dist and bower_components/weblogng-logger/release directories should now contain both minified and unminified versions of the libraries that can be included in the application.

Option 2 - NPM
^^^^^^^^^^^^^^
Install the WeblogNG logger library via `npm <https://www.npmjs.com/>`_ using::

   npm install angular-weblogng --save

The node_modules/angular-weblogng/dist and node_modules/weblogng-logger/release/ directories should now contain both minified and unminified versions of the libraries that can be included in the application.

Option 3 - Manual
^^^^^^^^^^^^^^^^^

#. `download <https://github.com/weblogng/weblogng-client-javascript/blob/master/release/logger.js>`__ the latest version of the WeblogNG logger from `GitHub <https://github.com/weblogng/weblogng-client-javascript/tree/master/release>`__ ; note the angular-weblogng library depends-on the WeblogNG Javascript client library and will not work without it
#. `download <https://github.com/weblogng/angular-weblogng/blob/master/dist/angular-weblogng.js>`__ the latest version of the WeblogNG module for AngularJS from `GitHub <https://github.com/weblogng/angular-weblogng/blob/master/dist>`__
#. include the logger.js and angular-weblogng.js files in your web application

The angular-weblogng and logging libraries should be downloaded and then included in your web application.
WeblogNG recommends combining the libraries with the page's other
javascript files so that an additional http request is not necessary. For
*development* purposes, the latest release can be pulled-in from github with::

    <script src="https://rawgit.com/weblogng/weblogng-client-javascript/master/release/logger.js"></script>
    <script src="https://rawgit.com/weblogng/angular-weblogng/master/dist/angular-weblogng.js"></script>

Add the WeblogNG module to the application
------------------------------------------

First, add the 'weblogng' module to the application and declare a `weblogngConfig` constant specifying the application's WeblogNG api key and an options hash with at least the application's name.  You can find or generate an api key on the `account page <https://www.weblogng.com/app/#/account>`_.

Example app configuration with the WeblogNG module::

    angular.module('yourAppModule', [
        'weblogng'
      ])
      .constant('weblogngConfig', {
        apiKey: 'your api key',
        options: {
          publishNavigationTimingMetrics: true,
          publishUserActive: true,
          application: 'your application name'
        }
      })


Please see the :ref:`WeblogNG Javascript Client Library<ref-client-library-javascript>` documentation for details on all the available Javascript library options.

Automatic instrumentation
-------------------------

With angular-weblogng the following aspects of the application can be measured automatically

* automatic measurement and reporting of application load time -- enable by specifying publishNavigationTimingMetrics: true
* automatic measurement of the number of active users -- enable by specifying publishUserActive: true
* automatic measurement and reporting of requests made with the standard AngularJS `$http service <https://docs.angularjs.org/api/ng/service/$http>`_ -- enabled automatically

Custom instrumentation
----------------------

The angular-weblogng module provides an instance of weblogng.Logger that can be injected into AngularJS components using the name, `$weblogng`.

This means application developers can inject `$weblogng` and then use all of the logging functions described in the :ref:`WeblogNG Javascript Client Library<ref-client-library-javascript>` documentation.


Create dashboard and charts with your application data
------------------------------------------------------

#. run your app, executing code timed with the angular-weblogng module; this will report raw metric data to the WeblogNG api
#. `create a dashboard <https://www.weblogng.com/app/#/dashboard>`_ and add a chart with your data

References
==========

* `AngularJS Module for WeblogNG <https://github.com/weblogng/angular-weblogng>`_ on GitHub
* :ref:`WeblogNG Javascript Client Library <ref-client-library-javascript>` documentation
* `AngularJS $http service <https://docs.angularjs.org/api/ng/service/$http>`_

