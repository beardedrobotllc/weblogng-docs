.. _ref-client-library-ios:
.. meta::
   :description: Technical documentation for the WeblogNG iOS client library for real user monitoring.
   :keywords: WeblogNG iOS Objective-C objc library
   :title: WeblogNG iOS Client Library

******************
iOS Client Library
******************

.. contents::

Overview
========

WeblogNG provides an iOS client library for iOS 6, 7, and 8 that is used by the application to measure and report data in real-world usage.

Usage
=====

Using the iOS client library follows an easy three-step process:

#. add the WNGLogger library dependency to your project
#. integrate the WNGLogger library into your application
#. create dashboard and charts with your metrics

Add the library to your project
-------------------------------

The WNGLogger library is available via CocoaPods and is the recommended installation path.

You can install it by adding a WNGLogger dependency to your Podfile:

#. Add the WNGLogger pod to your application's Podfile::

    pod 'WNGLogger', :git => 'https://github.com/weblogng/weblogng-client-iOS.git', :tag => '0.9.2'

#. execute ``pod install``.  There should be some output like::

    $ pod install
    Analyzing dependencies
    Pre-downloading: `WNGLogger` from `https://github.com/weblogng/weblogng-client-iOS.git`, tag `0.9.2`
    Downloading dependencies
    Using AFNetworking (2.4.1)
    Using JRSwizzle (1.0)
    Installing WNGLogger 0.9.2
    Generating Pods project
    Integrating client project

Use the WNGLogger in your application
-------------------------------------

#. add the WNGLogger header file
#. instantiate the Logger object using your api key. You can find or generate an api key on the `account page <https://www.weblogng.com/app/#/account>`_
#. send metrics with the values recorded by your application
#. example code taken from the (super-simple) `WeblogNG Sample App for iOS <https://github.com/weblogng/weblogng-client-ios-sample-app>`_ for iOS::

    #import "WNGAppDelegate.h"

    #import <WNGLogger/logger.h>

    @implementation WNGAppDelegate

    - (BOOL)application:(UIApplication *)application 
            didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
        NSString *apiKey = @"specify your api key here";
        NSString *application = @"specify your application name here";

        [WNGLogger initSharedLogger:apiKey application:application];

        [self someIntensiveLogic];

        //time execution of an arbitrary block
        [[WNGLogger sharedLogger] executeWithTiming:@"sample-app-anExpensiveBlock" aBlock:^(void){
            int millis_to_sleep = 250 + arc4random_uniform(250);
            float seconds_to_sleep = ((float) millis_to_sleep) / 1000;
            [NSThread sleepForTimeInterval:seconds_to_sleep];
        }];

        return YES;
    }

    - (void) someIntensiveLogic {
        NSString *metricName = @"sample-app-someIntensiveLogic";
        [[WNGLogger sharedLogger] recordStart:metricName];

        int millis_to_sleep = 500 + arc4random_uniform(250);
        float seconds_to_sleep = ((float) millis_to_sleep) / 1000;

        [NSThread sleepForTimeInterval:seconds_to_sleep];

        [[WNGLogger sharedLogger] recordFinishAndSendMetric:metricName];
    }

    @end

Create dashboard and charts with your application data
------------------------------------------------------

#. run your app, executing code timed with the library; this will report raw metric data to the WeblogNG api
#. `create a dashboard <https://www.weblogng.com/app/#/dashboard>`_ and add a chart with your data

Automatic measurement and logging of HTTP requests
--------------------------------------------------

The WNGLogger library supports *automatic* measurement and logging of HTTP requests made with NSURLConnection, including requests made by 3rd-party libraries.  The automatic HTTP request logging can be enabled by:

#. import the WNGLogging category for NSURLConnection via ``#import <WNGLogger/NSURLConnection+WNGLogging.h>``
#. invoke ``[NSURLConnection wng_enableLogging];``

For example, the application delegate's code above becomes::

    #import "WNGAppDelegate.h"

    #import <WNGLogger/logger.h>
    #import <WNGLogger/NSURLConnection+WNGLogging.h> //add WNGLogging category to NSURLConnection

    @implementation WNGAppDelegate

    - (BOOL)application:(UIApplication *)application 
            didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
        NSString *apiKey = @"specify your api key here";
        NSString *application = @"specify your application name here";

        [WNGLogger initSharedLogger:apiKey application:application];
        [NSURLConnection wng_enableLogging]; //enable logging of all requests; that's it!

        // perform other post-launch-activities

        return YES;
    }

    @end

The WNGLogging category uses the excellent JRSwizzle_ library to integrate the WNGLogger library with NSURLConnection and measure request execution times wherever requests might be made within the application.  If you would like to learn more about swizzling in Objective-C, we recommend reading NSHipster's explanation of `method swizzling`_.

References
==========

* `WeblogNG iOS Client Library <https://github.com/weblogng/weblogng-client-iOS>`_ on GitHub
* `WeblogNG iOS Sample App <https://github.com/weblogng/weblogng-client-ios-sample-app>`_ on GitHub
* `Specify the version of a CocoaPod Using git <http://guides.cocoapods.org/using/the-podfile.html#from-a-podspec-in-the-root-of-a-library-repo>`_
* JRSwizzle_ on Github
* NSHipster on `method swizzling`_ in Objective-C

.. _JRSwizzle: https://github.com/rentzsch/jrswizzle
.. _method swizzling: http://nshipster.com/method-swizzling/
