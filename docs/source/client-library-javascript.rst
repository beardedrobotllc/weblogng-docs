.. _ref-client-library-javascript:
.. meta::
   :description: Technical documentation for the WeblogNG Javascript client library for real user monitoring.
   :keywords: WeblogNG Javascript library
   :title: WeblogNG Javascript Client Library

*************************
Javascript Client Library
*************************

.. contents::

Overview
=========================

WeblogNG provides a Javascript client library for platforms with WebSocket support that is used by the application to measure and report data in real-world usage.

.. hint::
    The `Javascript sample app (Plunker) <http://embed.plnkr.co/Qm61MCcjJVJmwqlp5xP4/preview>`_ demonstrates everything documented here in a fully-interactive way.

Usage
=========================

Using the WeblogNG Javascript client is very easy:

#. add the WeblogNG Logger library dependency to your project
#. integrate the WeblogNG Logger into your application
#. create dashboard and charts with your metrics

Add the library to your project
-------------------------------

Option 1 - Bower
^^^^^^^^^^^^^^^^

Install the WeblogNG logger library via `bower <http://bower.io>`_ using::

   bower install weblogng-logger --save

The bower_components/weblogng-logger/release directory should now contain both minified and unminified versions of the library.

Option 2 - NPM
^^^^^^^^^^^^^^
Install the WeblogNG logger library via `npm <https://www.npmjs.com/>`_ using::

   npm install weblogng-logger --save

The node_modules/weblogng-logger/release/ directory should now contain both minified and unminified versions of the library.


Option 3 - Manual
^^^^^^^^^^^^^^^^^

#. `download <https://github.com/weblogng/weblogng-client-javascript/blob/master/release/logger.js>`_ the latest version of the WeblogNG logger from `GitHub <https://github.com/weblogng/weblogng-client-javascript/tree/master/release>`_
#. include the logger.js file in your web application

The logging library should be downloaded and then included in your web application.
WeblogNG recommends combining the logging library with the page's other
javascript files so that an additional http request is not necessary. For
*development* purposes, the latest release can be pulled-in from github with::

    <script src="https://rawgit.com/weblogng/weblogng-client-javascript/master/release/logger.js"></script>

Use the Logger in your application
----------------------------------

First, instantiate the Logger object using your api key and store it someplace convenient. You can find or generate an api key on the `account page <https://www.weblogng.com/app/#/account>`_.

The logger may be configured with an options dictionary, specifying:

* application: a String containing the name of the application, will be used as a namespace for metrics and events
* publishNavigationTimingMetrics: a boolean value, true means that metrics for the following operations will be recorded for the page automatically when the browser supports the `Navigation Timing API <http://caniuse.com/#feat=nav-timing>`_:
    * dns lookup
    * first byte
    * response recv
    * page load
* publishUserActive: a boolean value, true means that a 'user_active' event will be recorded in WeblogNG for each minute that mouse movement (mousemove) or key press (keyup) Javascript events occur

Example usage::

    var logger = new weblogng.Logger('api.weblogng.com', 'specify your api key here', {
          application: 'www'
          , publishNavigationTimingMetrics: true
          , publishUserActive: true
        });

Second, use the logging library to measure operations and send the recorded values to the api.

The WeblogNG logging library supports three approaches for measuring operations so that you can write the least amount of code necessary,
depending on the situation.

Option 1 - Use recordStart and recordFinish
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The simplest way to measure an operation is to use the Logger's recordStart and recordFinishAndSendMetric functions.
When using these functions, the Logger will:
#. create a Timer for the metric name specified in recordStart
#. retain it until recordFinishAndSendMetric (or recordFinish) is called
#. send the metric data to the api when sendMetric is called

.. caution::
    A Logger instance will only track one operation for a given metric name at a time, so only use
    recordStart and recordFinishAndSendMetric in situations where a single instance of the operation will
    be executing.  If recordStart and recordFinish are used to measure operations where there is concurrency,
    timing data will be lost and/or corrupted. When concurrent measurement of an operation with a given name
    is needed, use Timer objects directly or executeWithTiming, instead.

Example usage::

    $('#measureOperationBtn-simple').bind('click', function() {
      console.log('start simple operation ' + new Date());
      logger.recordStart('operation-simple');

      //simulate executing a function that will take some time
      setTimeout(function() {
        //now inside the completion callback for our successful 'operation'; record success!
        logger.recordFinishAndSendMetric('operation-simple');
        console.log('finish simple operation ' + new Date());
        updateStatusMessage('executed and measured operation-simple');
      }, generateRandomDelay());

      });


Option 2 - Use Timer objects directly
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

WeblogNG Timer objects can be created and used directly and then sent to the WeblogNG api via a logger instance.
WeblogNG recommends using timer objects directly when there is a possibility for concurrent execution of
the operation being measured as this approach is concurrency-safe.

Example usage::

    $('#measureOperationBtn-useTimerDirectly').bind('click', function() {
      console.log('start use timer directly for operation ' + new Date());
      var timer = new Timer();
      timer.start();

      //simulate executing a function or an async request that will take some time
      setTimeout(function() {
        //now inside the completion callback for our successful 'operation'; record success!
        timer.finish();
        logger.sendMetric('operation-useTimerDirectly', timer.getElapsedTime());
        console.log('finish use timer directly for operation ' + new Date());

        updateStatusMessage('executed and measured operation-useTimerDirectly');
      }, generateRandomDelay());

    });


Option 3 - Use executeWithTiming
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Logger#executeWithTiming function will execute the function provided as an argument with timing
automatically added and sent to the WeblogNG api.  Logger#executeWithTiming is concurrency-safe.
Internally, executeWithTiming will:

#. create a timer
#. execute the provided function
#. send the resulting metric to the api using the provided metric name
#. return the result or propagate the Error thrown by the function

Example usage::

    $('#measureOperationBtn-executeWithTiming').bind('click', function() {
      var function_to_exec = function() {
        console.log('start function_to_exec ' + new Date());
        setTimeout(function() {
          //now inside the completion callback for our successful 'operation'; record success!
          console.log('finish function_to_exec ' + new Date());
          updateStatusMessage('executed and measured operation-executeWithTiming');
        }, generateRandomDelay());

      };

      logger.executeWithTiming('operation-executeWithTiming', function_to_exec);
    });


Create dashboard and charts with your application data
------------------------------------------------------

#. run your app, executing code timed with the library; this will report raw metric data to the WeblogNG api
#. `create a dashboard <https://www.weblogng.com/app/#/dashboard>`_ and add a chart with your data

References
==========

* `WeblogNG Javascript Client Library <https://github.com/weblogng/weblogng-client-javascript>`_ on GitHub
* `WeblogNG Javascript Sample App <http://embed.plnkr.co/Qm61MCcjJVJmwqlp5xP4/preview>`_  on Plunker

