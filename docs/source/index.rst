Welcome to WeblogNG's Technical Documentation!
==============================================

WeblogNG enables developers and operations engineers to understand the performance of mobile and html/javascript applications,
as experienced by the end-user in production.

.. toctree::
   :maxdepth: 2

   architectural-overview
   client-libraries
   client-library-ios
   client-library-javascript
   client-library-angular-weblogng


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

